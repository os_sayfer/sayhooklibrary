package com.example.sayhooklib;

import android.util.Log;
import android.webkit.WebView;

import com.swift.sandhook.SandHook;
import com.swift.sandhook.annotation.HookClass;
import com.swift.sandhook.annotation.HookMethod;
import com.swift.sandhook.annotation.HookMethodBackup;
import com.swift.sandhook.annotation.MethodParams;

import java.lang.reflect.Method;

@HookClass(WebView.class)
//@HookReflectClass("android.app.Activity")
public class ActivityHooker {

    static urlArrayBuilder newUrlArrayBuilder = new urlArrayBuilder();
    static String[] urlBlackList = newUrlArrayBuilder.getUrlArray();

    @HookMethodBackup("loadUrl")
    @MethodParams(String.class)
    static Method loadUrlBackup;


    @HookMethod("loadUrl")
    @MethodParams(String.class)
    public static void loadUrl(WebView thiz, String str) throws Throwable {
        Log.e("ActivityHooker", "hooked loadUrl success");
        for (int i = 0; i <= urlBlackList.length-1;i++) {
            if (str.equals(urlBlackList[i])) {
                Log.e("ActivityHooker", "loadUrlHook: malicious url found: " + str);
                str = "https://sayfer.io/";
                i = urlBlackList.length;
            }
        }
        SandHook.callOriginByBackup(loadUrlBackup, thiz, str);
    }
}
