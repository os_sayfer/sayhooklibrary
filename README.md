- In order to use the library Add the JitPack repository to your build file

  Add it in your root build.gradle at the end of repositories:

	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}


- Add the dependency

	dependencies {
	        implementation 'com.gitlab.researcher8173:sayhooklibrary:LATEST_RELEASE'
	}


- To initialize the library register "Initialize" class as application by adding 
    android:name="YOUR_CLASS_PATH.Initialize" 
  to your manifest under application tag,
  or extend "Initialize" class with a class that already extend application.

